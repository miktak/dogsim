public class test {
    public static void main(String[] args)
    {
        Human bob = new Human("Bob");
        Dog bobert = new Dog("Bobert");

        boolean success = true;
        while(success)
        {
            success = bob.doWork();
        }

        boolean s2 = true;
        while(s2)
        {
            s2 = bob.feed(bobert);
        }
    }
}
