public class Human
{
    private String name;
    private int money;
    private int hunger;
    private int boredom;

    public Human(String name)
    {
        this.name = name;
        this.money = 0;
        this.hunger = 0;
        this.boredom = 0;
    }

    public boolean doWork()
    {
        if(hunger < 50 && boredom < 50)
        {
            money += 20;
            hunger += 5;
            boredom +=10;
            System.out.println("Succesful work day! Earned a nice 20");
            return true;
        }
        else
        {
            System.out.println(name + " is too tired or bored to work");
            return false;
        }
    }

    public void eat()
    {
        this.hunger -= 30;
        if(this.hunger < 0)
        {
            this.hunger = 0;
        }
        System.out.println(this.name + " is eating. Yum.");
    }

    public boolean feed(Dog dog)
    {
        if(this.money >= 50)
        {
            this.money -= 50;
            System.out.println(this.name + " bought food. You're sad but you know it's necessary");
            eat();
            dog.eat();
            return true;
        }
        else
        {
            System.out.println("You're too poor to buy food. Go work. Go be productive.");
            return false;
        }
    }

    public void play()
    {
        System.out.println("Human is now playing with dog.");
        this.boredom -= 30;
        if(this.boredom < 0)
        {
            this.boredom = 0;
        }
        this.hunger += 5;
    }
}