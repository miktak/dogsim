public class Dog {
    private String name;
    private int energy;
    private int hunger;
    private int boredom;

    public Dog(String name)
    {
        this.name = name;
        this.energy = 0;
        this.hunger = 0;
        this.boredom = 0;
    }

    public boolean takeNap()
    {
        if(this.hunger > 50)
        {
            System.out.println("Your dog is too hungry for a nap");
            return false;

        }
        else if(this.boredom > 50)
        {
            System.out.println("Your dog is too bored for a nap");
            return false;
        }
        else
        {
            System.out.println("Your dog is now taking a nap");
            this.energy += 20;
            this.hunger += 5;
            this.boredom += 10;
            return true;
        }
    }
    public void play()
    {
        System.out.println("Your dog played with the human.");
        this.boredom -= 30;
        if(this.boredom < 0)
        {
            this.boredom = 0;
        }
    }
    public boolean playWith(Human human)
    {
        if(this.energy < 50)
        {
            System.out.println("Your dog doesn't have enough energy to play");
            return false;
        }
        else
        {
            this.energy -= 50;
            this.play();
            human.play();
            return true;
        }
    }

    public void eat()
    {
        this.hunger -= 30;
        if(this.hunger < 0)
        {
            this.hunger = 0;
        }
        System.out.println(this.name + " is eating. Yum.");
    }
}