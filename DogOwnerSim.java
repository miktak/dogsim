public class DogOwnerSim {

    public static void main(String[] args) {
        Human bob = new Human("Bob");
        Dog dog = new Dog("Sparky");
        boolean success;
        boolean success2;
        boolean s1;
        boolean s2;

        for(int i = 0; i < 5; i++)
        {

            success = true;
            success2 = true;
            s1 = true;
            s2 = true;

            while(success)
            {
                success = dog.takeNap();
            }

            while(success2)
            {
                success2 = dog.playWith(bob);
            }

            while(s1)
            {
                s1 = bob.doWork();
            }

            while(s2)
            {
                s2 = bob.feed(dog);
            }
        }
    }

}

    
